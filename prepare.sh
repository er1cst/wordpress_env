#!/usr/bin/env bash

MYSQL_USER='wordpress'

prompt()
{
    PROMPT=$1
    echo -n "[ * ]   $1"   
}

prompt_done()
{
    echo $'\r''[done]'
}

prompt_error()
{
    echo $'\r''[error]'
}

echo 'make a copy of the docker-compose.yml'

cp docker-compose.yml.tmpl docker-compose.yml

# retrieve passwords for mysql users from random.org
prompt 'getting random passwords from random.org...'
PASSWORDS=$(curl --connect-timeout 10 --silent 'https://www.random.org/strings/?num=2&len=20&digits=on&upperalpha=on&loweralpha=on&unique=on&format=plain&rnd=new')
if [[ $? != 0 ]]; then
    prompt_error
    echo "unable to get random passwords at random.org! please check your internet connection."
    exit
fi
prompt_done

MYSQL_ROOT_PASSWORD=$(echo -n $PASSWORDS | cut -f 1 -d ' ')
MYSQL_PASSWORD=$(echo -n $PASSWORDS | cut -f 2 -d ' ')

sed -i "s!MYSQL_USER: .*\$!MYSQL_USER: $MYSQL_USER!" ./docker-compose.yml
sed -i "s!MYSQL_ROOT_PASSWORD: .*\$!MYSQL_ROOT_PASSWORD: $MYSQL_ROOT_PASSWORD!" ./docker-compose.yml
sed -i "s!MYSQL_PASSWORD: .*\$!MYSQL_PASSWORD: $MYSQL_PASSWORD!" ./docker-compose.yml

prompt "downloading the latest version of Wordpress..."
curl --silent -o wordpress.tar.gz 'https://wordpress.org/latest.tar.gz'
if [[ $? != 0 ]]; then
    echo "unable to download Wordpress! Please check your internet connection."
    prompt_error
    exit
fi 
prompt_done

prompt "extracting source codes to ./src/ ..."
if [ ! -d "./src/" ]; then
    mkdir -p ./src/
fi

tar --strip-components=1 -C ./src/ -zxf wordpress.tar.gz wordpress/ 2>/tmp/script_error
if [[ $? != 0 ]]; then
    prompt_error
    cat /tmp/script_error
    exit
fi
prompt_done

prompt "configuring Wordpress..."
cp ./src/wp-config-sample.php ./src/wp-config.php
sed -i "s/\(define('DB_NAME',\).*);/\1 'wordpress');/"           ./src/wp-config.php
sed -i "s/\(define('DB_USER',\).*);/\1 '$MYSQL_USER');/"         ./src/wp-config.php
sed -i "s/\(define('DB_PASSWORD',\).*);/\1 '$MYSQL_PASSWORD');/" ./src/wp-config.php
sed -i "s/\(define('DB_HOST',\).*);/\1 'mysql');/"               ./src/wp-config.php


replace=$(curl -s 'https://api.wordpress.org/secret-key/1.1/salt/')
if [[ $? != 0 ]]; then
    prompt_error
    echo "unable to retrieve secret key and salts from Wordpress API."
    exit
fi
# see: http://stackoverflow.com/questions/407523/escape-a-string-for-a-sed-replace-pattern
# see: https://stackoverflow.com/questions/29613304/is-it-possible-to-escape-regex-metacharacters-reliably-with-sed
IFS= read -d '' -r < <(sed -e ':a' -e '$!{N;ba' -e '}' -e 's/[&/\]/\\&/g; s/\n/\\&/g' <<<"$replace")
replacement=${REPLY%$'\n'}

sed -i "/\/\*\*#@\+/,/\/\*\*#@\-\*\//{/\/\*\*#@\-\*\// s/.*/$replacement/; t; d}" /home/er1cst/workspace/docker/wordpress/src/wp-config.php

prompt_done

prompt "do some cleaning..."
rm -rf /tmp/script_error
rm -rf ./wordpress.tar.gz
prompt_done
echo "Now you can run `docker-compose up -d` to run Wordpress!"
